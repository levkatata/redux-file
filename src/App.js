import React, { useState } from 'react';
import styles from './app.module.css';

import * as actions from './redux/actions';
import { connect } from 'react-redux';

function App({selectedFile, setSelectedFile, removeFile}) {
  const [refInput, setRefInput] = useState("");

  const handleClear = () => {
    removeFile();
    refInput.value = '';
  }

  return (
    <div className={styles.divmain}>
      <input ref={ref => setRefInput(ref)}
        type='file' 
        onChange={(e) => { 
          setSelectedFile(URL.createObjectURL(e.target.files[0]));
          console.log(e.target.files[0]); 
          }} />
      <button onClick={handleClear}>Clear</button>
      <img src={selectedFile}/>
    </div>
  );
}

const mapStateToProps = (state) => {
  console.log("From map:");
  console.log(state);
  return {selectedFile: state.file}
};

const mapDispatchToProps = (dispatch) => ({
  setSelectedFile: (file) => dispatch(actions.setFile(file)),
  removeFile: () => dispatch(actions.setFile(""))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

//export default App;
