import { createAction } from "@reduxjs/toolkit";

export const setFile = createAction('setfile');
export const clearFile = createAction('clearfile');