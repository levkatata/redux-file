import { createReducer } from '@reduxjs/toolkit';
import * as actions from './actions';

export const filesReducer = createReducer("", {
    [actions.setFile]: (state, action) => {
        console.log(state);
        console.log(action);
        return action.payload;
    },
    [actions.clearFile]: () => {
        console.log('message');
        return "";
    },
});